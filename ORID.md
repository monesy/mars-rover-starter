Objective:
1.Code Review，在区良同学的帮助下我知道了之前step的test会在后续编码过程中出错，并且我需要注意空指针异常，即及时进行判空处理。
2.Unit Test，我学习了单元测试的基本步骤，Test description的格式，Given，When，Then。
3.TDD测试驱动开发，由测试案例与结果去驱动编码，可以减少代码的多余与错误。每次的test或者feat后都要进行git。

Reflective：
进步，充实。

Interpretive：
我感觉今天学到了很多有用的东西，单元测试与TDD能够帮助我在今后的工作中减少很多麻烦，提高代码质量与工作效率，减轻测试人员的工作。

Decisional：
我决定要多多适应测试驱动开发，将这个开发思想应用到生活中。
