package com.afs.tdd;

import java.util.List;

public class MarsRover {
    private Location location;

    public MarsRover(Location location) {

        this.location = location;
    }


    public Location executeCommand(Command command) {
        if (command == Command.Move) {
            switch (location.getDirection()) {
                case South:
                    location.setLocationY(location.getLocationY() - 1);
                    break;
                case North:
                    location.setLocationY(location.getLocationY() + 1);
                    break;
                case West:
                    location.setLocationX(location.getLocationX() - 1);
                    break;
                case East:
                    location.setLocationX(location.getLocationX() + 1);
                    break;
            }
        }

        if (command == Command.TurnLeft) {
            switch (location.getDirection()) {
                case East:
                    location.setDirection(Direction.North);
                    break;
                case South:
                    location.setDirection(Direction.East);
                    break;
                case West:
                    location.setDirection(Direction.South);
                    break;
                case North:
                    location.setDirection(Direction.West);
                    break;
            }
        }

        if (command == Command.TurnRight) {
            switch (location.getDirection()) {
                case East:
                    location.setDirection(Direction.South);
                    break;
                case South:
                    location.setDirection(Direction.West);
                    break;
                case West:
                    location.setDirection(Direction.North);
                    break;
                case North:
                    location.setDirection(Direction.East);
            }
        }

        return this.location;
    }

    public Location executeCommand(List<Command> command){
        for (Command value : command) {
            executeCommand(value);
        }
        return this.location;
    }
}
