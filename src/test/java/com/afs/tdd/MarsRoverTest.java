package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MarsRoverTest {
    //-------------------------------------Face the North------------------------------------------------
    @Test
    void should_change_location_y_plus1_when_executeCommand_given_location_00N_command_Move() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        Location currentLocation = marsRover.executeCommand(Command.Move);

        Assertions.assertEquals(0, currentLocation.getLocationX());
        Assertions.assertEquals(1, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.North, currentLocation.getDirection());
    }

    @Test
    void should_change_direction_to_West_when_executeCommand_given_location_00N_command_TurnLeft() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        Location currentLocation = marsRover.executeCommand(Command.TurnLeft);

        Assertions.assertEquals(0, currentLocation.getLocationX());
        Assertions.assertEquals(0, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.West, currentLocation.getDirection());
    }

    @Test
    void should_change_direction_to_East_when_executeCommand_given_location_00N_command_TurnRight() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        Location currentLocation = marsRover.executeCommand(Command.TurnRight);

        Assertions.assertEquals(0, currentLocation.getLocationX());
        Assertions.assertEquals(0, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.East, currentLocation.getDirection());
    }

    //-------------------------------------Face the west------------------------------------------------
    @Test
    void should_change_location_x_cut1_when_executeCommand_given_location_00W_command_Move() {
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        Location currentLocation = marsRover.executeCommand(Command.Move);

        Assertions.assertEquals(-1, currentLocation.getLocationX());
        Assertions.assertEquals(0, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.West, currentLocation.getDirection());
    }

    @Test
    void should_change_direction_to_South_when_executeCommand_given_location_00W_command_TurnLeft() {
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        Location currentLocation = marsRover.executeCommand(Command.TurnLeft);

        Assertions.assertEquals(0, currentLocation.getLocationX());
        Assertions.assertEquals(0, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.South, currentLocation.getDirection());
    }

    @Test
    void should_change_direction_to_North_when_executeCommand_given_location_00W_command_TurnRight() {
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        Location currentLocation = marsRover.executeCommand(Command.TurnRight);

        Assertions.assertEquals(0, currentLocation.getLocationX());
        Assertions.assertEquals(0, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.North, currentLocation.getDirection());
    }

    //-------------------------------------Face the South------------------------------------------------
    @Test
    void should_change_location_y_cut1_when_executeCommand_given_location_00S_command_Move() {
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        Location currentLocation = marsRover.executeCommand(Command.Move);

        Assertions.assertEquals(0, currentLocation.getLocationX());
        Assertions.assertEquals(-1, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.South, currentLocation.getDirection());
    }

    @Test
    void should_change_direction_to_East_when_executeCommand_given_location_00S_command_TurnLeft() {
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        Location currentLocation = marsRover.executeCommand(Command.TurnLeft);

        Assertions.assertEquals(0, currentLocation.getLocationX());
        Assertions.assertEquals(0, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.East, currentLocation.getDirection());
    }

    @Test
    void should_change_direction_to_West_when_executeCommand_given_location_00S_command_TurnRight() {
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        Location currentLocation = marsRover.executeCommand(Command.TurnRight);

        Assertions.assertEquals(0, currentLocation.getLocationX());
        Assertions.assertEquals(0, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.West, currentLocation.getDirection());
    }

    //-------------------------------------Face the East------------------------------------------------
    @Test
    void should_change_location_x_plus1_when_executeCommand_given_location_00E_command_Move() {
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        Location currentLocation = marsRover.executeCommand(Command.Move);

        Assertions.assertEquals(1, currentLocation.getLocationX());
        Assertions.assertEquals(0, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.East, currentLocation.getDirection());
    }

    @Test
    void should_change_direction_to_North_when_executeCommand_given_location_00E_command_TurnLeft() {
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        Location currentLocation = marsRover.executeCommand(Command.TurnLeft);

        Assertions.assertEquals(0, currentLocation.getLocationX());
        Assertions.assertEquals(0, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.North, currentLocation.getDirection());
    }

    @Test
    void should_change_direction_to_South_when_executeCommand_given_location_00E_command_TurnRight() {
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        Location currentLocation = marsRover.executeCommand(Command.TurnRight);

        Assertions.assertEquals(0, currentLocation.getLocationX());
        Assertions.assertEquals(0, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.South, currentLocation.getDirection());
    }

    @Test
    void should_21E_when_executeCommand_given_location_00N_command_MRMMR() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        List<Command> commandList = new ArrayList<>();
        commandList.add(Command.Move);
        commandList.add(Command.TurnRight);
        commandList.add(Command.Move);
        commandList.add(Command.Move);
        commandList.add(Command.TurnRight);
        Location currentLocation = marsRover.executeCommand(commandList);

        Assertions.assertEquals(2, currentLocation.getLocationX());
        Assertions.assertEquals(1, currentLocation.getLocationY());
        Assertions.assertEquals(Direction.South, currentLocation.getDirection());
    }
}
